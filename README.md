# PROJET WEB LPIOT DESCARTES

## Le projet

Le projet se décompose en deux directories : le root qui est le directory des fichiers ouverts sur le serveur et le directory 'client' qui contient les fichiers ouverts chez le client.

Les fichiers ouverts chez le client sont les fichiers qui servent d'interface : le code React par exemple, purement du Front qui va afficher les données qu'on manipule côté serveur.

## Manipulation du projet

'yarn dev'

'npm start'

## Erreurs récurrentes

'EADDRINUSE' : 'killall node' corrige cette erreur

## HOW TO SETUP NODE.JS AND MYSQL

1. install node.js and npm

2. create your project directory

3. 'npm init' in the directory

4. install dependencies including mysql and express using 'npm install mysql'

5. install mysql-server

6. type 'mysql' when it's installed in command line

7. type this : " ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password'; "

8. you're done and you can now enable connection to your mysql server !


## 21/10/18

