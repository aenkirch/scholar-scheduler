import React, { Component } from 'react'
import axios from 'axios'
import moment from 'moment'
import Select from 'react-select'
//import 'moment/locale/zh-cn';
import Scheduler, { SchedulerData, ViewTypes, DATE_FORMAT } from 'react-big-scheduler'
import withDragDropContext from './withDnDContext'
import 'react-big-scheduler/lib/css/style.css'

class App extends Component { // EXAMINER CODE ET ADOPTER DRY (DON'T REPEAT YOURSELF) : ELIMINEZ LES REPETITIONS EN SUBDIVISANT EN COMPOSANTS (REACT MAP ?)
    constructor(props) {
        super(props)    // BURGER MENU ! Avec donc un componsant par truc à afficher en dessous de l'EDT
                    // RAJOUTER UN TITRE "INFOS SUR LE CRENEAU" avec deux appels à {this.state.prof} et {this.state.autreInfos} qui seront remplis dans le clic
        this.state = {
            viewModel: new SchedulerData("2018-10-18", ViewTypes.Week),
            allCreneaux: [],
            allProfs: [],
            allSalles: [],
            selectedFormation: {},
            selectedProf: '',
            selectedGroupe: '',
            formation: [],
            groupe: [],
            champFormation: '',
            champFormationLabel: '',
            champGroupe: '',
            allGroupes: [],
            allModules: [],
            allUE: [],
            selectedType: '',
            selectedUE: '',
            matieres: [],
            selectedMatiere: '',
            debutCreneau: '',
            finCreneau: '',
            champUE: '',
            champLabelUE: '',
            champMatiere: '',
            champLabelMatiere: '',
            champCouleurMatiere: '',
            champThemeMatiere: '',
            champTypeMatiere: ''
        }
    }

    componentDidMount = () => {
        axios.all([axios.get("/api/getFormations"),
        axios.get("/api/getAllGroupes"),
        axios.get("/api/getAllMatieres"),
        axios.get("/api/getAllCreneaux"),
        axios.get("/api/getAllProfs"),
        axios.get("/api/getAllSalles"),
        axios.get("/api/getAllModules"),
        axios.get("/api/fillFormUE")])
            .then(axios.spread((formationsRes, allGroupesRes, allMatieresRes, allCreneauxRes, allProfsRes, allSallesRes, allModulesRes, allUERes) => {
                this.setState({
                    formation: formationsRes.data,
                    allGroupes: allGroupesRes.data,
                    matieres: allMatieresRes.data,
                    allCreneaux: allCreneauxRes.data,
                    allProfs: allProfsRes.data,
                    allSalles: allSallesRes.data,
                    allModules: allModulesRes.data,
                    allUE: allUERes.data
                })
            }))
    }

    handleForm_groupe = (event) => {
        axios.post('/api/getGroupe', {
            id_form: event.id
        }).then((res) => {
            this.setState({ selectedFormation: event, groupe: res.data })
        })
    }

    loadAgendaData = (event) => {
        const viewModel = this.state.viewModel;

        axios.post('/api/getModule', {
            id_form: this.state.selectedFormation.id
        }).then((res) => {
            viewModel.setResources(res.data)
        })

        axios.post('/api/getCreneaux', {
            id_grpe: event.value
        }).then((res) => {
            res.data.forEach((element) => {
                element.start = moment.unix(element.start).format("YYYY-MM-DD HH:mm:ss");
                element.end = moment.unix(element.end).format("YYYY-MM-DD HH:mm:ss");
            })
            viewModel.setEvents(res.data)

            this.setState({ viewModel: viewModel, events: res.data, selectedGroupe: event });
        })
    }

    loadAgendaData_creneauModification = (event) => {
        const viewModel = this.state.viewModel;

        axios.post('/api/getCreneaux', {
            id_grpe: this.state.selectedGroupe.value
        }).then((res) => {
            res.data.forEach((element) => {
                element.start = moment.unix(element.start).format("YYYY-MM-DD HH:mm:ss");
                element.end = moment.unix(element.end).format("YYYY-MM-DD HH:mm:ss");
            })
            viewModel.setEvents(res.data)

            this.setState({ viewModel: viewModel, events: res.data });
        })
    }

    handleChange = (event) => {
        event.preventDefault();
        this.setState({ [event.target.name]: event.target.value });
    }

    createFormation = (event) => {
        event.preventDefault();
        axios.post('/api/createFormation', {
            nom: this.state.champFormation,
            id: this.state.formation[this.state.formation.length - 1].id + 1,
            label: this.state.champFormationLabel
        }).then((res) => {
            console.log(res.data)
        })
        this.componentDidMount();
    }

    createGroupe = (event) => {
        event.preventDefault();
        axios.post('/api/createGroupe', {
            id_grpe: this.state.allGroupes[this.state.allGroupes.length - 1].id_grpe + 1,
            num_grpe: this.state.champGroupe,
            id_promo: this.state.selectedFormation.id
        }).then((res) => {
            console.log(res.data)
        })
        this.componentDidMount();
    }

    createUE = (event) => {
        event.preventDefault();
        axios.post('/api/createUE', {
            id_grpe: this.state.allModules.length,
            id_form: this.state.selectedFormation.id,
            nom: this.state.champUE,
            label: this.state.champLabelUE
        }).then((res) => {
            console.log(res.data)
        })
        this.componentDidMount();
    }

    createCreneau = (event) => {
        event.preventDefault();
        axios.post('/api/createCreneau', {
            id_creneau: this.state.allCreneaux[this.state.allCreneaux.length - 1].id_creneau + 1,
            tDeb: moment(this.state.debutCreneau).format('X'),
            tFin: moment(this.state.finCreneau).format('X'),
            id_edth: '',
            id_mat: this.state.selectedMatiere.value,
            id_prof: this.state.selectedProf.value,
            id_grpe: this.state.selectedGroupe.value,
            id_salle: this.state.selectedSalle.value
        }).then((res) => {
            this.loadAgendaData_creneauModification();
        })
    }

    createMatiere = (event) => {
        event.preventDefault();
        axios.post('/api/createMatiere', {
            id_mat: this.state.matieres[this.state.matieres.length - 1].value + 1,
            id_ue: this.state.selectedUE.value,
            nom: this.state.champMatiere,
            label: this.state.champLabelMatiere,
            couleur: this.state.champCouleurMatiere,
            themes: this.state.champThemeMatiere,
            typeEns: this.state.champTypeMatiere
        })
    }

    render() {
        const viewModel = this.state.viewModel;
        return (
            <div>
                <div>
                    <Scheduler schedulerData={viewModel}
                        prevClick={this.prevClick}
                        nextClick={this.nextClick}
                        onSelectDate={this.onSelectDate}
                        onViewChange={this.onViewChange}
                        eventItemClick={this.eventClicked}
                        viewEventClick={this.ops1}
                        viewEventText="Sauvegarder"
                        viewEvent2Text="Supprimer"
                        viewEvent2Click={this.ops2}
                        updateEventStart={this.updateEventStart}
                        updateEventEnd={this.updateEventEnd}
                        moveEvent={this.moveEvent}
                        newEvent={this.newEvent}
                    />
                    <br />
                    <div style={{ width: 200, height: 200 }}>
                        <Select
                            onChange={this.handleForm_groupe}
                            options={this.state.formation}
                            placeholder="Choisissez votre formation..."
                        />
                        <br />
                        <Select
                            onChange={this.loadAgendaData}
                            options={this.state.groupe}
                            placeholder="Choisissez votre groupe..."
                        />
                        <br />
                    </div>

                    <h2>Création de formation</h2>
                    Entrez un nom de formation à créer : <br />
                    <input type="text" value={this.state.champFormation} onChange={e => this.handleChange(e)} name="champFormation" />
                    <br />
                    Entrez le label de la formation: <br />
                    <input type="text" value={this.state.champFormationLabel} onChange={e => this.handleChange(e)} name="champFormationLabel" />
                    <button onClick={(e) => this.createFormation(e)}>Valider</button> <br /> <br />

                    <h2>Création de groupe</h2>
                    <div style={{ width: 200 }}>
                        <Select
                            onChange={this.handleForm_groupe}
                            options={this.state.formation}
                            placeholder="Choisissez votre formation..."
                        />
                    </div>
                    Entrez le type du groupe à créer : <br />
                    <input type="text" value={this.state.selectedType} onChange={e => this.handleChange(e)} name="selectedType" /> <br />
                    Entrez le numéro du groupe à créer : <br />
                    <input type="text" value={this.state.champGroupe} onChange={e => this.handleChange(e)} name="champGroupe" /> <br />
                    <button onClick={(e) => this.createGroupe(e)}>Valider</button> <br /> <br />

                    <h2>Création d'UE</h2>
                    <div style={{ width: 200 }}>
                        <Select
                            onChange={this.handleForm_groupe}
                            options={this.state.formation}
                            placeholder="Choisissez votre formation..."
                        />
                    </div>
                    Entrez le nom de l'UE à créer : <br />
                    <input type="text" value={this.state.champUE} onChange={e => this.handleChange(e)} name="champUE" /> <br />
                    Entrez le label de l'UE à créer : <br />
                    <input type="text" value={this.state.champLabelUE} onChange={e => this.handleChange(e)} name="champLabelUE" /> <br />
                    <button onClick={(e) => this.createUE(e)}>Valider</button> <br /> <br />

                    

                    <h2>Creation de créneau (sélectionnez votre groupe en haut)</h2>
                    <div style={{ width: 400 }}>
                        <Select
                            onChange={e => this.setState({ selectedMatiere: e })}
                            options={this.state.matieres}
                            placeholder="Choisissez votre matière..."
                        />
                        <Select
                            onChange={e => this.setState({ selectedProf: e })}
                            options={this.state.allProfs}
                            placeholder="Choisissez le professeur..."
                        />
                        <Select
                            onChange={e => this.setState({ selectedSalle: e })}
                            options={this.state.allSalles}
                            placeholder="Choisissez la salle..."
                        />
                    </div>
                    Entrez le début du créneau : <br />
                    <input type="text" placeholder="ex : 2018-10-17 15:00:00" value={this.state.debutCreneau} onChange={e => this.handleChange(e)} name="debutCreneau" /> <br />
                    Entrez la fin du créneau : <br />
                    <input type="text" placeholder="ex : 2018-10-17 18:00:00" value={this.state.finCreneau} onChange={e => this.handleChange(e)} name="finCreneau" /> <br />
                    <button onClick={(e) => this.createCreneau(e)}>Valider</button> <br /> <br />

                </div>
            </div>
        )
    }

    prevClick = (schedulerData) => {
        schedulerData.prev();
        schedulerData.setEvents(this.state.events);
        this.setState({
            viewModel: schedulerData
        })
    }

    nextClick = (schedulerData) => {
        schedulerData.next();
        schedulerData.setEvents(this.state.events);
        this.setState({
            viewModel: schedulerData
        })
    }

    onViewChange = (schedulerData, view) => {
        schedulerData.setViewType(view.viewType, view.showAgenda, view.isEventPerspective);
        schedulerData.setEvents(this.state.events);
        this.setState({
            viewModel: schedulerData
        })
    }

    onSelectDate = (schedulerData, date) => {
        schedulerData.setDate(date);
        schedulerData.setEvents(this.state.events);
        this.setState({
            viewModel: schedulerData
        })
    }

    eventClicked = (schedulerData, event) => {
        console.log(event);
        axios.post('/api/getProf', {
            id_prof: event.id_prof
        }).then((res) => {
            console.log(res.data)
        })
        axios.post('/api/getMatiereInfos', {
            id_mat: event.id_mat
        }).then((res) => {
            console.log(res.data)
        })
    };

    ops1 = (schedulerData, event) => {
        axios.post('/api/saveEvent', {
            tDeb: moment(event.start).format('X'),
            tFin: moment(event.end).format('X'),
            id: event.id
        })
        this.loadAgendaData_creneauModification();
    };

    ops2 = (schedulerData, event) => {
        axios.post('/api/deleteEvent', {
            id: event.id
        }).then((res) => {
            this.loadAgendaData_creneauModification();
        })
    };

    newEvent = (schedulerData, slotId, slotName, start, end, type, item) => {
        let newFreshId = 0;
        schedulerData.events.forEach((item) => {
            if (item.id >= newFreshId)
                newFreshId = item.id + 1;
        });

        let newEvent = {
            id: newFreshId,
            title: 'New event you just created',
            start: start,
            end: end,
            resourceId: slotId,
            bgColor: 'purple'
        }
        schedulerData.addEvent(newEvent);
        this.setState({
            viewModel: schedulerData
        })
    }

    updateEventStart = (schedulerData, event, newStart) => {
        schedulerData.updateEventStart(event, newStart);
        this.setState({
            viewModel: schedulerData
        })
    }

    updateEventEnd = (schedulerData, event, newEnd) => {
        schedulerData.updateEventEnd(event, newEnd);
        this.setState({
            viewModel: schedulerData
        })
    }

    moveEvent = (schedulerData, event, slotId, slotName, start, end) => {
        console.log(event);
        schedulerData.moveEvent(event, slotId, slotName, start, end);
        console.log(event);
        this.setState({
            viewModel: schedulerData
        })
    }
}

export default withDragDropContext(App)